package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import sample.model.Book;

import java.io.File;
import java.util.Random;


public class MainScreenController {
    @FXML
    private TableView table;
    @FXML
    private TableColumn titleColumn;
    @FXML
    private TableColumn authorColumn;
    @FXML
    private TableColumn yearColumn;
    @FXML
    private ComboBox authorInput;
    @FXML
    private TextField isbnInput;
    @FXML
    private TextArea descriptionInput;
    @FXML
    private TextField coverImageInput;
    @FXML
    private DatePicker yearInput;
    @FXML
    private TextField titleInput;

    private ObservableList<Book> books;
    private FileChooser chooser = new FileChooser();
    private File file;

    public MainScreenController() {
        System.out.println("Controller constructor");
    }

    public void setBooks(ObservableList<Book> books) {
        this.books = books;
        bindTable();
    }

    @FXML
    public void initialize() {
        System.out.println("In innitController");
        bindTable();
    }

    private void bindTable() {
        if (books != null) {
            System.out.println("Bind Table. Book size: " + books.size());
            titleColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("title"));
            yearColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("year"));
            authorColumn.setCellValueFactory(new PropertyValueFactory<Book, String>("author"));
            table.setItems(books);
        }
    }

    public void newButton(ActionEvent actionEvent) {
        clearFields();
    }

    private void clearFields(){
        titleInput.setText("");
        authorInput.getSelectionModel().clearSelection();
        yearInput.getEditor().clear();
        isbnInput.setText("");
        descriptionInput.setText("");
        coverImageInput.setText("");
    }

    public void chooseButton(ActionEvent actionEvent) {
        File file = chooser.showOpenDialog(null);
        if(file != null && file.exists() && !file.isDirectory()){
            coverImageInput.setText(file.getAbsolutePath());
        } else if (this.file == null){
            coverImageInput.setText("");
        }
        if(file!=null){
            this.file=file;
        }
    }

    public void saveButton(ActionEvent actionEvent) {
        Book book = new Book(titleInput.getText(), "SomeBobby");
        book.setCoverImagePath(file.getAbsolutePath());
        book.setDescription(descriptionInput.getText());
        book.setIsbn(isbnInput.getText());
        book.setYear(yearInput.getValue());
        books.add(book);
        clearFields();
    }
}
