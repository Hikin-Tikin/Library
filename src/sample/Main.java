package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import sample.model.Book;

public class Main extends Application {

    private ObservableList<Book> bookObservableList = FXCollections.observableArrayList();

    public Main() {
        bookObservableList.add(new Book("Good day", "Bob"));
        bookObservableList.add(new Book("Bad day", "Mary"));
        bookObservableList.add(new Book("My book", "Me"));
        System.out.println("In Constructor");
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.out.println("Start");
//        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("layout/sample.fxml"));
        GridPane root = loader.load();
        MainScreenController controller = loader.getController();
        System.out.println("Controller retrieved");
        controller.setBooks(bookObservableList);
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 400, 375));
        primaryStage.show();
    }

    @Override
    public void init() throws Exception {
        super.init();
        System.out.println("Init");
    }

    public static void main(String[] args) {
        launch(args);
    }
}
